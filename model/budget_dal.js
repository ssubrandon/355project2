var mysql   = require('mysql');
var db  = require('./db_connection.js');

var connection = mysql.createConnection(db.config);

exports.get_all = function(callback) {
    var query = 'SELECT * FROM budget;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback)
{
    var query = "insert into budget (budget_name, amount) values (?, ?)";
    var query_data = [params.budget_name, params.amount];
    connection.query(query, query_data, function(err, res)
    {
        callback(err, res);
    })
};

exports.delete = function(params, callback)
{
    var query = "delete from budget where budget_id = ?";
    var query_data = params.budget_id;
    connection.query(query, query_data, function(err, res)
    {
        callback(err, res);
    })
};

exports.edit = function(params, callback)
{
    var query = "update budget set budget_name = ?, amount = ? where budget_id = ?";
    var query_data = [params.budget_name, params.amount, params.budget_id];
    connection.query(query, query_data, function(err, res)
    {
        callback(err, res);
    })
};

exports.get_info = function(params, callback)
{
    var query = "call budget_getinfo(?)";
    var query_data = params.budget_id;
    connection.query(query, query_data, function(err, res)
    {
        callback(err, res);
    })
};