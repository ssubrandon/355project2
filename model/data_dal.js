var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAllExpenses = function(callback) {
    var query = 'SELECT * FROM total_expense_view;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getIndividualExpenses = function(callback) {
    var query = 'SELECT sub.* from (select * from transactions) sub where sub.amount>500';
    console.log(query);

    connection.query(query,function(err, result) {
        callback(err, result);
    });
};