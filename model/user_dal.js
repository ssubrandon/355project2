var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view company_view as
 select s.*, a.street, a.zip_code from company s
 join address a on a.address_id = s.address_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT * FROM users;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(account_id, callback) {
    var query = 'SELECT u.*, b.budget_name, b.amount FROM users u ' +
        'LEFT JOIN account_budget ab on ab.account_id = u.account_id ' +
        'LEFT JOIN budget b on b.budget_id = ab.budget_id ' +
        'WHERE u.account_id = ?';
    var queryData = [account_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};


exports.insert = function(params, callback) {

    // FIRST INSERT THE user info
    var query = 'INSERT INTO users (fname, lname, email) VALUES (?,?,?)';

    var queryData = [params.fname,params.lname,params.email];

    connection.query(query, queryData, function(err, result) {

        // THEN USE THE account_ID RETURNED AS insertId AND THE SELECTED budget_IDs INTO budget_type
        var account_id = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO account_budget (account_id, budget_id) VALUES ?';

        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var userbudgetsData = [];
        if (params.budget_id.constructor === Array) {
            for (var i = 0; i < params.budget_id.length; i++) {
                userbudgetsData.push([account_id, params.budget_id[i]]);
            }
        }
        else {
            userbudgetsData.push([account_id, params.budget_id]);
        }

        // NOTE THE EXTRA [] AROUND companyAddressData
        connection.query(query, [userbudgetsData], function(err, result){
            callback(err, result);
        });
    });

};

exports.delete = function(account_id, callback) {
    var query = 'DELETE FROM users WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

//declaring to be used locally
var userBudgetInsert = function(account_id, budgetIdArray, callback){
    var query = 'INSERT INTO account_budget (account_id, budget_id) values ?'

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var accountBudgetData = [];
    if (budgetIdArray.constructor === Array) {
        for (var i = 0; i < budgetIdArray.length; i++) {
            accountBudgetData.push([account_id, budgetIdArray[i]]);
        }
    }
    else {
        accountBudgetData.push([account_id, budgetIdArray]);
    }
    connection.query(query, [accountBudgetData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.userBudgetInsert = userBudgetInsert;

//declare the function so it can be used locally
var userBudgetDeleteAll = function(account_id, callback){
    var query = 'DELETE FROM account_budget WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.userBudgetDeleteAll = userBudgetDeleteAll;

exports.edit_account = function(params, callback)
{
    var query = "update account set first_name = ?, last_name = ?, email = ? where account_id = ?";
    var query_data = [params.first_name, params.last_name, params.email, params.account_id];
    connection.query(query, query_data, function(err, res)
    {
        callback(err, res);
    })
};

exports.update = function(params, callback) {
    var query = 'UPDATE users SET fname = ?, lname = ?, email = ? WHERE account_id = ?';
    var queryData = [params.fname, params.lname, params.email];

    connection.query(query, queryData, function(err, result) {
        //delete company_address entries for this company
        userBudgetDeleteAll(params.account_id, function(err, result){

            if(params.budget_id != null) {
                //insert company_address ids
                userBudgetInsert(params.account_id, params.budget_id, function(err, result){
                    callback(err, result);
                });}
            else {
                callback(err, result);
            }
        });

    });
};

exports.edit = function(account_id, callback) {
    var query = 'CALL user_getinfo(?)';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.get_info = function(params, callback)
{
    var query = "call users_getinfo(?)";
    var query_data = params.account_id;
    connection.query(query, query_data, function(err, res)
    {
        callback(err, res);
    });
};

exports.get_user_budgets = function(callback)
{
    var query = "call user_getbudgetinfo(?)";
    var query_data = params.account_id;
    connection.query(query, query_data, function (err, result)
    {
        callback(err, result);
    })
};
