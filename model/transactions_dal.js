var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view company_view as
 select s.*, a.street, a.zip_code from company s
 join address a on a.address_id = s.address_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT * FROM transactions;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(trans_id, callback) {
    var query = 'SELECT t.*, u.fname, u.lname FROM transactions t ' +
        'LEFT JOIN users u on u.account_id = t.account_id ' +
        'WHERE t.trans_id = ?';
    var queryData = [trans_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};


exports.insert = function(params, callback) {

    // FIRST INSERT THE user info
    var query = 'INSERT INTO transactions (made_on, amount, description, type_of, account_id) VALUES (?,?,?,?,?)';

    var queryData = [params.made_on,params.amount,params.description, params.type_of, params.account_id];

    connection.query(query, queryData, function(err, result) {

        // THEN USE THE trans_ID RETURNED AS insertId AND THE SELECTED budget_IDs INTO budget_type
        var trans_id = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO transaction_budgets (trans_id, budget_id) VALUES ?';

        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var transactionbudgetsData = [];
        if (params.budget_id.constructor === Array) {
            for (var i = 0; i < params.budget_id.length; i++) {
                transactionbudgetsData.push([trans_id, params.budget_id[i]]);
            }
        }
        else {
           transactionbudgetsData.push([trans_id, params.budget_id]);
        }

        // NOTE THE EXTRA [] AROUND companyAddressData
        connection.query(query, [transactionbudgetsData], function(err, result){
            callback(err, result);
        })
    });

};

exports.delete = function(trans_id, callback) {
    var query = 'DELETE FROM transactions WHERE trans_id = ?';
    var queryData = [trans_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

//declaring to be used locally
var transactionBudgetInsert = function(trans_id, budgetIdArray, callback){
    var query = 'INSERT INTO transaction_budgets (trans_id, budget_id) values ?'

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var transactionBudgetData = [];
    if (budgetIdArray.constructor === Array) {
        for (var i = 0; i < budgetIdArray.length; i++) {
            transactionBudgetData.push([trans_id, budgetIdArray[i]]);
        }
    }
    else {
        transactionBudgetData.push([trans_id, budgetIdArray]);
    }
    connection.query(query, [transactionBudgetData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.transactionBudgetInsert = transactionBudgetInsert;

//declare the function so it can be used locally
var transactionBudgetDeleteAll = function(trans_id, callback){
    var query = 'DELETE FROM transaction_budgets WHERE trans_id = ?';
    var queryData = [trans_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.transactionBudgetDeleteAll = transactionBudgetDeleteAll;


exports.update = function(params, callback) {
    var query = 'UPDATE transactions SET made_on = ?, amount = ?, description = ?, type_of = ?, account_id = ? ' +
        'WHERE account_id = ?';
    var queryData = [params.made_on,params.amount,params.description, params.type_of, params.account_id];

   // var queryData = [params.fname, params.lname, params.email];

    connection.query(query, queryData, function(err, result) {
        //delete company_address entries for this company
        transactionBudgetDeleteAll(params.trans_id, function(err, result){

            if(params.budget_id != null) {
                //insert company_address ids
                transactionBudgetInsert(params.trans_id, params.budget_id, function(err, result){
                    callback(err, result);
                });}
            else {
                callback(err, result);
            }
        });

    });
};

exports.edit = function(trans_id, callback) {
    var query = 'CALL transaction_getinfo(?)';
    var queryData = [trans_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.get_info = function(params, callback)
{
    var query = 'CALL transaction_getinfo(?)';
    var queryData = params.trans_id;

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

