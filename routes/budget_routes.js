var express = require('express');
var router = express.Router();
var budget_dal = require('../model/budget_dal');

router.get('/', function(req, res)
{
    budget_dal.get_info(req.query, function(err, result)
    {
        if (err)
        {
            res.send(err);
        }
        else
        {
            res.render('budget/budgetViewById', {'budget': result[0][0]})
        }
    })
});

router.get('/all', function(req, res)
{
    budget_dal.get_all(function(err, result)
    {
        if (err)
        {
            res.send(err);
        }
        else
        {
            res.render('budget/budgetViewAll', {'result': result});
        }
    })
});

router.get('/add', function(req, res)
{
    res.render('budget/budgetAdd');
});

router.get('/insert', function(req, res)
{
    budget_dal.insert(req.query, function(err, result)
    {
        if (err)
        {
            res.send(err);
        }
        else
        {
            res.redirect(302, '/budget/all');
        }
    })
});

router.get('/delete', function(req, res)
{
    budget_dal.delete(req.query, function(err, result)
    {
        if (err)
        {
            res.send(err);
        }
        else
        {
            res.redirect(302, '/budget/all');
        }
    })
});

router.get('/edit', function(req, res)
{
    budget_dal.get_info(req.query, function(err, result)
    {
        res.render('budget/budgetUpdate', {budget: result[0][0]});
    })
});

router.get('/update', function(req, res)
{
    budget_dal.edit(req.query, function(err, result)
    {
        if (err)
        {
            res.send(err);
        }
        else
        {
            res.redirect(302, '/budget/all');
        }
    })
});

module.exports = router;