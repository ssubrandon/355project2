var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'CS 355 Project 2: Brandon Williams' });
});

module.exports = router;
