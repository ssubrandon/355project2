var express = require('express');
var router = express.Router();
var user_dal = require('../model/user_dal');
var budget_dal = require('../model/budget_dal');

//view all users
// View All companys
router.get('/all', function(req, res) {
    user_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('user/userViewAll', { 'result':result });
        }
    });
});

//by given id
router.get('/', function(req, res){
    if(req.query.account_id == null) {
        res.send('account_id is null');
    }
    else {
        user_dal.getById(req.query.account_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('user/userViewById', {'result': result});
            }
        });
    }
});

//return add a new user
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    budget_dal.get_all(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('user/userAdd', {'budget': result});
        }
    });
});

//insert page
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.fname == null) {
        res.send('First name must be provided.');
    }
    else if(req.query.lname == null){
        res.send('Last name myst be provided');
    }
    else if(req.query.email == null){
        res.send('An email must be entered')
    }
    else if(req.query.budget_id == null) {
        res.send('At least one budget must be selected');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        user_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/user/all');
            }
        });
    }
});

router.get('/edit', function(req, res)
{
        if(req.query.account_id == null){
            res.send('an account_id is required')
        }
        else{
            user_dal.edit(req.query.account_id, function(err, result){
                res.render('user/userUpdate', {user: result[0][0], budget: result[1]});
            });
        }
});

router.get('/update', function(req, res){
    user_dal.update(req.query, function(err, result){
        res.redirect(302, '/user/all');
    });
});

router.get('/delete', function(req, res){
    if(req.query.account_id == null) {
        res.send('account_id is null');
    }
    else {
       user_dal.delete(req.query.account_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/user/all');
            }
        });
    }
});

module.exports = router;