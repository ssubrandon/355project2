var express = require('express');
var router = express.Router();
var transactions_dal = require('../model/transactions_dal');
var budget_dal = require('../model/budget_dal');
var user_dal = require('../model/user_dal');

//view all transactions

router.get('/all', function(req, res) {
    transactions_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('transaction/transactionViewAll', { 'result':result });
        }
    })
});

//by given id
router.get('/', function(req, res){
    if(req.query.trans_id == null) {
        res.send('trans_id is null');
    }
    else {
        transactions_dal.getById(req.query.trans_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('transaction/transactionViewById', {'result': result});
            }
        });
    }
});

//return add a new user
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    budget_dal.get_all(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            user_dal.getAll(function(err2, result2){
                if(err2){
                    res.send(err2);
                }
                else{
                    res.render('transaction/transactionAdd', {'budget': result, 'user': result2});

                }
            });
        }
    });
});

//insert page
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.made_on == null) {
        res.send('Date must be provided.');
    }
    else if(req.query.amount == null){
        res.send('Amount must be provided');
    }
    else if(req.query.description == null){
        res.send('Description must be entered')
    }
    else if(req.query.account_id == null){
        res.send('an account must be entered')
    }
    else if(req.query.budget_id == null) {
        res.send('At least one budget must be selected');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        transactions_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/transaction/all');
            }
        })
    }
});

router.get('/edit', function(req, res)
{
    if(req.query.trans_id == null){
        res.send('a trans_id is required')
    }
    else{
        transactions_dal.edit(req.query.trans_id, function(err, result){
            user_dal.getAll(function(err2, result2) {
                res.render('transaction/transactionUpdate', {
                    transaction: result[0][0],
                    budget: result[1],
                    user: result2
                });
            });
        });
    }
});

router.get('/update', function(req, res){
    transactions_dal.update(req.query, function(err, result){
        res.redirect(302, '/transaction/all');
    });
});

router.get('/delete', function(req, res){
    if(req.query.trans_id == null) {
        res.send('trans_id is null');
    }
    else {
        transactions_dal.delete(req.query.trans_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/transaction/all');
            }
        });
    }
});

module.exports = router;