var express = require('express');
var router = express.Router();
var data_dal = require('../model/data_dal');

router.get('/main', function(req, res) {
    res.render('data/dataHome');
});

router.get('/view', function(req, res) {
    data_dal.getAllExpenses(function(err, result) {
        if(err){
            res.send(err);
        }
        else{
            res.render('data/dataView', {'result':result});

        }
    });
});

router.get('/over500', function(req, res){
        data_dal.getIndividualExpenses(function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('data/dataMonthlyExpenseView', {'result': result});
            }
        });
});

module.exports = router;